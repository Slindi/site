# Henkilökohtainen sivusto / My personal website (in english down below)

Sivustolla on käytetty Bootstrap-pohjaa. Tein myös CI/CD-putken sivuille, eli kun päivitän sivujen version Gitlabin repositoryyn, putki tekee sivustosta imagen ja työntää sen Gitlabin konttirekisteriin samoin kuin Googlen pilven konttirekisteriin ja deployaa sivuston Kubernetes-klusteriin varustettuna staattisella IP:llä. Sivusto löytyy osoitteesta http://35.205.246.172 (offline ATM)

I've used Bootstrap template for the design because why not!
Also, I've set up CI/CD-pipeline, so when I commit a new version or change something on my site in the Gitlab repo, the pipe will make an image out of the site, push it to Gitlab's container registry for my project (the site), push the image to Google Cloud's container registry and deploy the container into Kubernetes cluster with static IP. Site can be found at http://35.205.246.172 (offline ATM)