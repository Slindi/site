*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
Test Firefox
    Open Browser   http://35.205.246.172/    Firefox
    Title Should Be     Sami Lindgren
    Close Browser

Test Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome      chrome_options=${chrome_options}
    Go To   http://35.205.246.172/
    Title Should Be     Sami Lindgren: portfolio
    Close Browser
